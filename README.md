# ez430_chronos
Playing around with an ez430 chronos dev watch from TI. 

I'm currently building with the "915MHz - Limited CCS Core Edition" 
configuration to account for the 32K code limitation in the free version
of CCS, so note that a few files are vaulted in static libraries.
Specifically, several files modified within /driver are part of
ez430_chronos_drivers.lib, yet the changes *are not* built into
the lib. 