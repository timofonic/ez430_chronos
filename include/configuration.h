/*
 * CONFIGURATION.H
 * 	Created on: Oct 31, 2015
 *  Author: Ben Sherman
 *
 * 	This file is used to select configuration
 * 	values that I've added in.
 *
 *	10-31-15	Init.
 */

#ifndef INCLUDE_CONFIGURATION_H_
#define INCLUDE_CONFIGURATION_H_

// A value of '1' enables beeps when buttons are pressed
// A value of '0' disables beeps when the buttons are pressed
// Use '0' as the default because I don't like button press beeps.
#define BUTTON_PRESS_BEEP 	(0)

#endif /* INCLUDE_CONFIGURATION_H_ */
