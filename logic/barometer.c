/*
 * BAROMETER.C
 * 	Created on: Oct 31, 2015
 *  Author: Ben Sherman
 *
 *	Barometer functionality. Modeled off of (and uses a lot of) altitude.c/.h
 *
 *	10-31-15	Init.
 */

// *************************************************************************************************
// Include section

// system
#include "project.h"

// driver
#include "altitude.h"
#include "barometer.h"
#include "display.h"

// *************************************************************************************************
// Prototypes section

// *************************************************************************************************
// Defines section

// *************************************************************************************************
// Global Variable section

// *************************************************************************************************
// Extern section
// Use struct alt sAlt that's defined in altitude.c and declared extern in altitude.h

// *************************************************************************************************
// @fn          sx_barometer
// @brief       Stub barometer sx function
// @param       u8 line		LINE1
// @return      none
// *************************************************************************************************
void sx_barometer(u8 line)
{
    // Function can be empty
    // Restarting of altitude measurement will be done by subsequent full display update
}

// *************************************************************************************************
// @fn          display_barometer
// @brief       Barometer display function Displays in mmHg or inHg.
// @param       u8 line		LINE1
//             	u8 update   DISPLAY_LINE_UPDATE_FULL, DISPLAY_LINE_UPDATE_PARTIAL, DISPLAY_LINE_CLEAR
// @return      none
// *************************************************************************************************
void display_barometer(u8 line, u8 update)
{
    u8 *str;

    // redraw whole screen
    if (update == DISPLAY_LINE_UPDATE_FULL)
    {
        // Enable pressure measurement
        sAlt.state = MENU_ITEM_VISIBLE;

        // Start measurement
        start_altitude_measurement();

        // Display altitude
        display_barometer(LINE1, DISPLAY_LINE_UPDATE_PARTIAL);
    }
    else if (update == DISPLAY_LINE_UPDATE_PARTIAL)
    {
        // Update display only while measurement is active
        if (sAlt.timeout > 0)
        {
            if (sys.flag.use_metric_units)
            {
                // Display barometric pressure in xxx mmHg format, allow 1 leading blank digits
                str = int_to_array(get_pressure_mmHg(), 4, 1);
            }
            else
            {
                // Display altitude in xx.xx inHg format, allow 0 leading blank digits
                str = int_to_array(get_pressure_inHg_x100(), 4, 0);

                // Turn on the decimal indicator
                display_symbol(LCD_SEG_L1_DP1, SEG_ON);
            }
            display_chars(LCD_SEG_L1_3_0, str, SEG_ON);
        }
    }
    else if (update == DISPLAY_LINE_CLEAR)
    {
        // Disable pressure measurement
        sAlt.state = MENU_ITEM_NOT_VISIBLE;

        // Stop measurement
        stop_altitude_measurement();

        if (!sys.flag.use_metric_units)
        {
        	// Turn off decimal
            display_symbol(LCD_SEG_L1_DP1, SEG_OFF);
        }
    }
}

// *************************************************************************************************
// @fn          mx_barometer
// @brief       Mx button handler to call mx_altitude, as that function handles the offset.
// @param       u8 line         LINE1
// @return      none
// *************************************************************************************************
void mx_barometer(u8 line)
{
	mx_altitude(line);
}

// Convert the sensor's native pascal pressure reading to mmHg
s16 get_pressure_mmHg()
{
	// 1 Pa = 0.00750061683 mmHg
	return (sAlt.pressure * 0.00750061683);
}

// Convert the sensors' native pascal pressure reading to inHg
s16 get_pressure_inHg_x100()
{
	// 1Pa = .000295 inHg
	// Multiply this value by 100 to make handling
	// the trailing decimals easier
	return (sAlt.pressure * 0.0295);
}
