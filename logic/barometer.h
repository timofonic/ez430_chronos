/*	BEN SHERMAN CHANGES
 * 	11-01-2015	First rev. Based off of altitude.h
 */

#ifndef BAROMETER_H_
#define BAROMETER_H_

// *************************************************************************************************
// Include section

// *************************************************************************************************

// menu functions
extern void sx_barometer(u8 line);
extern void mx_barometer(u8 line);
extern void display_barometer(u8 line, u8 update);
extern s16 	get_pressure_mmHg();
extern s16 	get_pressure_inHg_x100();

// *************************************************************************************************
// Defines section
#define BAROMETER_MEASUREMENT_TIMEOUT    ALTITUDE_MEASUREMENT_TIMEOUT

// *************************************************************************************************
// Extern section

#endif                                             /*BAROMETER_H_ */
